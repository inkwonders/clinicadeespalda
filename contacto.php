<?php
if (isset($_POST['name']) && isset($_POST['email']) && isset($_POST['message'])) {
  $name = $_POST["name"];
  $email = $_POST["email"];
  $message = $_POST["message"];


  function test_input($data)
  {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
  }


  ini_set('display_errors', 1);
  error_reporting(E_ALL);
  $from = "$email";
  // $to = "contactodiagnostico@newrospine.com.mx,merlion@inkwonders.com";
  $to = "contactodiagnostico@newrospine.com.mx,eddy@inkwonders.com,merlion@inkwonders.com";
  $subject = "Contacto desde formulario de contacto de Clinica de Espalda";
  $message = "Nombre: " . "$name" . "<br/><br/> Correo: " . "$email" . "<br/><br/> Mensaje: " . $message;

  $headers = "MIME-Version: 1.0" . "\r\n";
  $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
  $headers .= "From: Clínica de espaldas NewroSpine <contacto@clinicadeespalda.com>";


  mail($to, $subject, $message, $headers);
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Dr. Víctor Hugo Malo</title>
  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
  <link href="assets/css/agradecimiento.css" rel="stylesheet">

    <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-176069940-2"></script>
  <script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  

  gtag('config', 'UA-176069940-2');
  </script>
</head>

<body>
  <div id="topbar" class="d-none d-lg-flex align-items-center fixed-top boxshadtop">
    <div class="logotipo_img">
      <a href="index.html" class="logoespine"> <img src="/assets/img/web/clinica.svg" alt="" width="60" height="60"></a>
    </div>

    <div class="container-fluid d-flex">
      <div class="contact-info mr-auto">
        <div class="row">
          <div class="col custom_col_1">
            <i class="icofont-google-map"></i> <span class="info_contacto_malo">Hacienda Vegil No. 102, Jardines de
              la Hacienda, 76180 Santiago de
              Querétaro, Qro. </span>
          </div>

          <div class="col custom_col_1">
            <i class="icofont-ui-clock"></i> <span class="info_contacto_malo"> Lun - Vie : 8:00 a 20:00 <br>
              Sab - Dom: 9:00 a 17:00</span>
          </div>

          <div class="col">
            <i class="icofont-phone"></i>
            <span class="info_contacto_malo">Llámanos al </span>
            <br>
            <span>
              <strong class="contacto_malo_strong"> 442 215 4558 </strong>
            </span>

          </div>

        </div>
      </div>
    </div>
  </div>
  <div class="agradecimientos">
    <div class="container-fluid">
      <div class="row">
        <div class="col">
          <div class="imagen">
            <h1>¡Gracias por ponerte en contacto con nosotros, <span class="nombrecliente"> <?php echo $name ?></span>!</h1>
            <br>
            <h2 style="margin-left: 0 !important;">Nos pondremos en contacto contigo en breve.</h2>
            <br>
            <br>
            <span>puedes volver al inicio <a style="padding:0 !important; margin: 0 !important;" href="index.html"> Aquí </a> <br> o espera 5 segundos para que la página lo haga automáticamente... </span>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script>
    // Usando setTimeout para ejecutar una función después de 5 segundos.
    setTimeout(function() {
      // Redirigir con JavaScript
      window.location.href = 'index.html';
    }, 5000);
  </script>
</body>

</html>